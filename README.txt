Dynamic Menus module

PREREQUISITES

Drupal 5.0 and client-side JavaScript (uses jQuery).

OVERVIEW

The Dynamic Menus module provides a dynamic menu block for each menu
provided by the menu module.  You can then display the dynamic menu
blocks instead of the normal menu blocks.

In a Dynamic Menu, clicking on a collapsed arrow icon will open that
sub-tree; clicking again will close it. Any sub-tree that starts open
(ie: the active trail) always remains open.

DISCUSSION

I am unsure of the best UI interface for opening/closing menus. I find
flyover menus incredibly irritating. Clicking on little +/- icons is
annoying because they are so small. My solution for now is to make
clicking on a parent item open/close its sub-menu and offer the parent
item's link as its own first child. This seems fine for admins but may
confuse users. OTOH, multi-level menus for novice users probably
aren't a good idea anyway. Suggestions welcome.

AUTHOR

Barry Jaspan
firstname at lastname dot org
