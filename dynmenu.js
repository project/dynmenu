
$(function() {
    /*
      A tree that starts expanded should remain expanded for the life of
      the page, so we only operate on li.collapsed.
    */
    $("ul.dynmenu li.collapsed")

	/* Hide all child <ul>s. */
	.find("ul").css("display", "none").end()

	/*
	  Below, we're putting a toggle handler on <li>s which contain
	  <ul>s which contain leaf <li>s.  When an <a> is clicked,
	  we don't want the parent <li>'s toggle handler invokved.
	*/
	.find("a").click(function(e) { e.stopPropagation(); }).end()

	/*
	  When an <li> is clicked, toggle its class and its child
	  <ul>'s display.
	*/
	.toggle(function() {
	    $(this).removeClass("collapsed").addClass("expanded")
		.find("> ul").css("display", "block");
	}, function() {
	    $(this).removeClass("expanded").addClass("collapsed")
		.find("> ul").css("display", "none");
	})
});

